# -*- coding: utf-8 -*-
"""Module for the MolDX abstraction."""
import csv


class MolDXMedCode:
    """Class representing the MolDX medical coding system."""

    def __init__(self, logger, datafile_pth):
        """Initialize the instance.

        :param logger: A logger for the plugin to use.
        :param datafile_pth: The full path to the data file containing the
                             mapping of test codes to names.
        """
        self._logger = logger
        self._datafile_pth = datafile_pth
        # the data for this plugin is just a mapping of codes to names and thus
        # a dict is sufficent
        self._mappings = {}
        self._ingest()

    @property
    def logger(self):
        """Property getter for self._logger.

        :return: The configured logger.
        """
        return self._logger

    @property
    def datafile_pth(self):
        """Property getter for self._datafile_pth.

        :return: The configured data file path.
        """
        return self._datafile_pth

    def contains_result_mappings(self):
        """Return a boolean for if this coding system contains result mappings.

        NOTE: This is inherant to the coding system and not to the plugin. Some
              coding systems (e.g. LOINC) contain both test codes and result
              codes that the tests map to. Some don't (e.g. MolDX).
        :return: True if the coding system contains result mappaings or False
                 otherwise.
        """
        # MolDX does not contain reult mappings.
        return False

    def get_name_for_test_code(self, test_code):
        """Return the name of the test for a given test code.

        :param test_code: The test code.
        :return: The name of the test for the code, or None if the name is not
                 found.
        """
        return self._mappings.get(test_code, None)

    def get_test_code_for_name(self, name):
        """Return the test code for a given test name.

        NOTE: This is a strict string match, so name must be as it exists in
              the data file. Capitalization, whitespace, etc are all
              significant.

        :param name: The name of the test.
        :return: The test code for the name or None if the name is not found.
        """
        # there are a number of ways to get the key for a vlue in a dict, this
        # one is more code, but way less time complexity and extra space
        keys = list(self._mappings.keys())
        vals = list(self._mappings.values())

        code = None
        try:
            code = keys[vals.index(name)]
        except ValueError as ve:
            self._logger.warning(
                f"Could not find test code for test name {name}. {ve}"
            )

        return code

    def get_test_code_mappings(self):
        """Return a copy of the code to name mappings for this coding system.

        NOTE: Returing a copy so external entities can't mutate our truth.
        :return: A copy of our test code to test name mappings.
        """
        return self._mappings.copy()

    def get_test_to_result_mappings(self):
        """Return the mapping of test codes to result codes for this system.

        NOTE: This is only meaningful for systems that contain result
              mappings. It is also for growth as we don't have a coding system
              that has mappings yet.
        """
        # for later growth, *and* this system doesn't support result mappings
        # anyway
        return None

    def _ingest(self):
        """Read the configured data file and setup the mappings.

        NOTE: it is expected that this file has no header. No lines are
              skipped.
        """
        with open(self._datafile_pth) as f:
            reader = csv.reader(f, skipinitialspace=True, quotechar='"')
            self._mappings = dict(reader)
