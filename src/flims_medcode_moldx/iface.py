# -*- coding: utf-8 -*-
"""Main interface for the FracturedLIMS MolDX medical coding system plugin."""

from flims_medcode_moldx.moldx import MolDXMedCode

_PLUGIN_NAME = "FracturedLIMS MolDX Medical Coding System Plugin"

# TODO: this isn't really a plugin. This whole package is the plugin. This is
#       the object that holds the abstraction in the plugin. rename before
#       going live.
_plugin = None


# non-public
def _get_plugin():
    """Return the plugin object.

    :return: The plugin.
    :raises ValueError: If the plugin has not been initialized before an
                        attempt to use.
    """
    global _plugin
    if _plugin is None:
        raise ValueError(
            f"{_PLUGIN_NAME} is not yet configured but an attempt is "
            f"being made to use it. You must call configure in the "
            f"interface module first."
        )

    return _plugin


# public interface


def configure(logger=None, datafile_pth=None):
    """Configure the plugin.

    :param logger: The logger the plugin will use.
    :param datafile_pth: The path to the datafile containing test codes to
                         names of tests.
    """
    global _plugin

    if _plugin is None:
        # not allowing configuration more than once
        logger.debug(f"Configuring {_PLUGIN_NAME}")
        _plugin = MolDXMedCode(logger=logger, datafile_pth=datafile_pth)


def get_name():
    """Get the name of the moldx plugin.

    :return: the name of the moldx plugin.
    """
    return _PLUGIN_NAME


def contains_result_mappings():
    """Return True if the coding system contains result mappings, else False.

    :return: Return True if the coding system contains result mappings, else
             False.
    """
    return _get_plugin().contains_result_mappings()


def get_name_for_test_code(test_code):
    """Return the name of the test for a given test code.

    :param test_code: The test code.
    :return: The name of the test for the code, or None if the name is not
             found.
    """
    return _get_plugin().get_name_for_test_code(test_code)


def get_test_code_for_name(name):
    """Return the test code for a given test name.

    NOTE: This is a strict string match, so name must be as it exists in
          the data file. Capitalization, whitespace, etc are all
          significant.

    :param name: The name of the test.
    :return: The test code for the name or None if the name is not found.
    """
    return _get_plugin().get_test_code_for_name(name)


def get_test_code_mappings():
    """Return a copy of the code to name mappings for this coding system.

    :return: A copy of our test code to test name mappings.
    """
    return _get_plugin().get_test_code_mappings()


def get_test_to_result_mappings():
    """Return the mapping of test codes to result codes for this system.

    NOTE: This is only meaningful for systems that contain result
          mappings. It is also for growth as we don't have a coding system
          that has mappings yet.
    """
    return _get_plugin().get_test_to_result_mappings()
