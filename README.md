# FracturedLIMS MolDX Medical Coding System Plugin

This is the MolDX medical coding system plugin for FracturedLims. This plugin
reads a MolDX data file containing medical codes and names and makes the 
information available to FracturedLIMS in the standard method the application
expects.

This plugin DOES NOT provide the actual code data file (nor does FracturedLIMS)
due to licensing issues. For testing purposes, the plugin can be pointed to 
a simple CSV file where each rows consists of `code, name` entries. No header
is expected.

This plugin also represents the simplest possible medical coding system plugin
for FracturedLIMS. MolDX proper only contains test codes and names, with no
information about the shape of results expected for a given test code. As such,
all the plugin can do is to read the data file and give a name back for a given
code. Result description specification must be handled manually in 
FracturedLIMS.
 
Medical coding system plugins expose no blueprints and have no need for 
database access of any kind. The only requirement for them is that the expected
interface is implemented in the `iface` module. Outside of this there are no
requirements on the architecture of the plugins.

